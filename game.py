# Imports the library needed for random numbers
from random import randint

# Asks for the user's name
name = input("Hi! What is your name? ")

months = [
"January", "February",
"March", "April",
"May","June",
"July", "August",
"September", "October",
"November", "December"
    ]

month_min = 1
month_max = 12
day_min = 1
day_max = 31
year_min = 1924
year_max = 2004

stored_date = 0
stored_month = 0
stored_year = 0

correct_date = False
correct_month = False
correct_year = False

# Gives the computer 5 guesses before the code ends
for guess_number in range(1, 6):

    # Initializes the ranges of the birth month and year that the computer will be guessing
    birth_month = randint(month_min, month_max)
    birth_date = randint(day_min, day_max)
    birth_year = randint(year_min, year_max)

    if correct_date == True:
        birth_date = stored_date

    if correct_month == True:
        birth_month = stored_month

    if correct_year == True:
        birth_year = stored_year


    # Guesses a random birthday and asks the user if its correct
    print("Guess:", guess_number, name, "were you born in", months[birth_month - 1], "/", birth_date, "/", birth_year, "?")
    answer_1 = input("Is the year correct? (type 'yes' or 'no'): ")
    if answer_1 == 'no':
        answer_2 = input("Is it earlier or later? (type 'earlier' or later): ")
        if answer_2 == 'earlier':
            year_max = birth_year - 1

        if answer_2 == 'later':
            year_min = birth_year + 1

    elif answer_1 == 'yes':
        correct_year = True
        stored_year = birth_year
        print("Great, now let's ask you some more questions.")

    answer_3 = input("Is the month correct? (type 'yes' or 'no'): ")
    if answer_3 == 'no':
        answer_4 = input("Is it earlier or later? (type 'earlier' or later): ")
        if answer_4 == 'earlier':
            month_max = birth_month - 1

        if answer_4 == 'later':
            month_min = birth_month + 1

    elif answer_3 == 'yes':
        correct_month = True
        stored_month = birth_month
        print("Great, now let's ask you some more questions.")
    answer_5 = input("Is the day correct? (type 'yes' or 'no'): ")
    if answer_5 == 'no':
        answer_6 = input("Is it earlier or later? (type 'earlier' or later): ")
        if answer_6 == 'earlier':
            day_max = birth_date - 1


        if answer_6 == 'later':
            day_min = birth_date + 1

    elif answer_5 == 'yes':
        correct_date = True
        stored_date = birth_date
        print("Great, thanks for your input.")



    # Checks whether the computer guessed correctly
    if correct_date == True and correct_month == True and correct_year == True:
        print("I knew it! I guessed your birthday!")
        exit()

    # Exits the code with a unique message on the 5th guess
    elif guess_number == 5:
        print("I have better things to do. Good bye.")

    # If not correct, prints a message and returns to the start of the loop to make another guess
    else:
        print("Drat! Let me try again!")
